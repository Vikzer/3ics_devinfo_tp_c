/**
 * @file glouton.c
 * @author Victor Minon
 * @version 1.0
 * @date 2021-10-22
 * 
 * @copyright Copyright (c) 2021
 */

#include "../../include/glouton.h"


/**
 * @brief sort by decreasing insertion of object in an array
 * @param tab : array that will be sorted
 * @param size : Table size
 */
void insertion_sort_descending(Object array[], int size)
{
   int j;
   for (int i = 1; i < size; i++)
   {
      Object object = array[i];
      j = i - 1;
      while (j >= 0 && (object.cost/object.weight < array[j].cost/array[j].weight))
      {
            array[j + 1] = array[j];
            j = j - 1;
      }
      array[j + 1] = object;
   }
}

/**
 * @brief this function resolve the knapsack problem
 * @param item : array list of item
 * @param size : size of the item list
 * @param weight : backpack weight
 * @return Object* : List of item in the knapsack
 */
Object *knapsack(Object item[],int size,int weight){

    insertion_sort_descending(item,size);

    int sum=0;
    Object *ret=malloc(100*sizeof(Object));

    for (int i = 0; i < size; i++)
    {
        if (sum+(item[i].weight)<=weight)
        {
            sum+=item[i].weight;
            ret[i]=item[i];
        }
    }
    return ret;
}