/**
 * @file dychotomy.c
 * @author Victor Minon
 * @version 1.0 
 * @date 2021-10-21
 * 
 * @copyright Copyright (c) 2021
 */

#include "../../include/dychotomy.h"

/**
 * @brief Search a value in an array and returns his index
 * @param array : Array to be treated 
 * @param size_t : Size of the array
 * @param value : wanted value
 * @return int : Index of the wanted value
 */
int find_by_dichotomy(int array[], int size_t, int value ){

    int half;
    int inferior  = 0;
    int superior = size_t - 1;
    int indexRet = -1;

    while (indexRet==-1)
    {
        half = (inferior+superior)/2;

        if (array[half]==value)
        {
            indexRet=half;
        }
        else
        {
            if (value < array[half])
            {
                superior=half-1;
            }
            else
            {
                inferior=half+1;
            }
        }
    }  

return indexRet;
}