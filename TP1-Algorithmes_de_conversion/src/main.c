#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../include/function.h"

int main(int argc, char const *argv[])
{
	int resultint=0;
	char *resultchar=NULL;
	unsigned int valint=0;
	char *valchar=NULL;
	int choix=0;
	int bit=0;

	printf("Liste des conversions :\n");
	printf("1. Binaire -> Décimal\n");
	printf("2. Hexadécimal -> Décimal\n");
	printf("3. Décimal -> Hexadécimal (v1)\n");
	printf("4. Décimal -> Hexadécimal (v2)\n");
	printf("5. Décimal -> Binaire (v1)\n");
	printf("6. Décimal -> Binaire (v2)\n");
	printf("7. Décimal -> Binaire (v3)\n");
	printf("Choisissez une conversion a effectuer : ");
	scanf("%d",&choix);
	
	switch (choix)
	{
	case (1):

		//Binaire -> Décimal (bintodec)

		printf("Entrez le nombre de Bit de votre nombre binaire : ");
		scanf("%d",&bit);
		valchar = malloc(sizeof(bit)* sizeof(char));

		printf("Entrez un nombre en base 2 : ");
		scanf("%s",valchar);

		resultint=bintodec(valchar);

		printf("Le nombre binaire \"%s\" correspond a \"%d\" en décimal\n",valchar,resultint);

		break;

	case (2): 
	
		//Hexadécimal -> Décimal (hexatodec)

		printf("Entrez le nombre de caractère de votre nombre hexadécimal : ");
		scanf("%d",&bit);
		valchar = malloc(sizeof(bit)* sizeof(char));

		printf("Entrez un nombre en base 16 : ");
		scanf("%s",valchar);

		resultint=hexatodec(valchar);

		printf("Le nombre hexadécimal \"%s\" correspond a \"%d\" en décimal\n",valchar,resultint);

		break;
	
	case (3): 
	
		//Decimal -> Hexadécimal (v1) (dectohexa_v1)

		printf("Entrez un nombre en base 10 : ");
		scanf("%u",&valint);

		resultchar=dectohexa_v1(valint);

		printf("Le nombre décimal \"%u\" correspond a \"%s\" en hexadécimal\n",valint,resultchar);

		break;

	case (4): 
	
		//Decimal -> Hexadécimal (v2) (dectobin_v2)

		printf("Entrez un nombre en base 10 : ");
		scanf("%u",&valint);

		resultchar=dectohexa_v2(valint);

		printf("Le nombre décimal \"%u\" correspond a \"%s\" en hexadécimal\n",valint,resultchar);

		break;

	case (5): 
	
		//Decimal -> Binaire (v1) (dectobin_v1)

		printf("Entrez un nombre en base 10 : ");
		scanf("%u",&valint);

		resultchar=dectobin_v1(valint);

		printf("Le nombre décimal \"%u\" correspond a \"%s\" en binaire\n",valint,resultchar);

		break;
	
	case (6): 
	
		//Decimal -> Binaire (v2) (dectobin_v2)

		printf("Entrez un nombre en base 10 : ");
		scanf("%u",&valint);

		resultchar=dectobin_v2(valint);

		printf("Le nombre décimal \"%u\" correspond a \"%s\" en binaire\n",valint,resultchar);

		break;

	case (7): 
	
		//Decimal -> Binaire (v3) (dectobin_v3)

		printf("Entrez un nombre en base 10 : ");
		scanf("%u",&valint);

		resultchar=dectobin_v2(valint);

		printf("Le nombre décimal \"%u\" correspond a \"%s\" en binaire\n",valint,resultchar);

		break;

	
	default:
		break;
	}
}
	