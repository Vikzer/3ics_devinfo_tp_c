#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char *inverse(char *a)
{
    int j = 0;
    char *tab = malloc(strlen(a)*sizeof(char));

    for(int i=strlen(a)-1; i>=0; i--){
        tab[j++] = a[i];
    }
    tab[j] = '\0';
    return tab;
}

int bintodec(char *val)
{

    int taille=0;
    int coef=1;
    int convert=0;

    taille = strlen(val);

    /*while (val[taille]!='\0')
    {
        taille ++;
    }
    */
    
    for (int i = taille-1; i>=0; i--)
    {
        convert+=(val[i]-48)*coef;
        coef*=2;
    }
    
    return convert;
}

int hexatodec(char *val)
{

    int taille=0;
    int coef=1;
    int convert=0;

    taille = strlen(val);

    /*while (val[taille]!='\0')
    {
        taille ++;
    }
    */
    
    for (int i = taille-1; i>=0; i--)
    {
        if('A' == val[i] || 'a' == val[i])
        {
            convert+=10*coef;
        }
        else if('B' == val[i]|| 'b' == val[i])
        {
            convert+=11*coef;
        }
        else if('C' == val[i]|| 'c' == val[i])
        {
            convert+=12*coef;
        }	
        else if('D' == val[i]|| 'd' == val[i])
        {
            convert+=13*coef;
        }
        else if('E' == val[i]|| 'e' == val[i])
        {
            convert+=14*coef;
        }
        else if('F' == val[i]|| 'f' == val[i])
        {
            convert+=15*coef;
        }
        else 
        {
            convert+=(val[i]-48)*coef;
        }
        coef*=16;
    }
    return convert;

}


char *dectohexa_v1(unsigned int val)
{
    char *convert= NULL;
    convert = malloc(sizeof(val) * sizeof(char));
    int rest=0,i=0;

    while (val!=0)
    {
        rest = val % 16;
        val = val / 16;

        switch (rest)
        {
        case 10:
            convert[i]='A';
            break;

        case 11:
            convert[i]='B';
            break;

        case 12:
            convert[i]='C';
            break;

        case 13:
            convert[i]='D';
            break;

        case 14:
            convert[i]='E';
            break;

        case 15:
            convert[i]='F';
            break;

        default:
            convert[i]= rest + '0';
            break;
        }
        i++;
    }
    char *inversconvert=inverse(convert);
    return inversconvert;
}

char *dectobin_v1(unsigned int val)
{
    char *resultat= NULL;
    int puissance = 1, taille = 0, tempVal1 = val, nombrePuissance = 0, n = 0;

    // Nombre de chiffre dans le nombre : 
    if (tempVal1 >= 10)
    {
        while(tempVal1 > 0){
            taille ++;
            tempVal1 = tempVal1 / 10 ;
        }
    }
    else {
        taille = 1;
    }

    resultat = malloc((taille * 4) * sizeof(char));

    if (resultat == NULL) // On vérifie si l'allocation a marché ou non
    {
        exit(0); // On arrête tout
    }

    while (val - puissance >= puissance)
    {
        puissance *= 2;
        nombrePuissance ++;
    }

    while (nombrePuissance >= 0)
    {   
        puissance = 1;
        for (int i = 0; i < nombrePuissance; i++)
        {
            puissance *= 2;
        }
        if(val >= puissance){
            resultat[n] = '1';
            val = val - puissance;
            nombrePuissance--;
            n++;
        }
        else
        {
            resultat[n] = '0';
            nombrePuissance--;
            n ++;
        }
        
    }
    
    return resultat;
    free(resultat);
}


char *dectobin_v2(unsigned int val){
    char *resultat = NULL, *inversionResultat = NULL;
    int taille = 0, tempValTaille1 = val, reste = 0, i = 0;

    
    if (tempValTaille1 >= 10)
    {
        while(tempValTaille1 > 0){
            taille ++;
            tempValTaille1 = tempValTaille1 / 10 ;
        }
    }
    else {
        taille = 1;
    }

    resultat = malloc((taille * 4) * sizeof(char));
    inversionResultat = malloc((taille * 4) * sizeof(char));

    if (resultat == NULL || inversionResultat == NULL) // On vérifie si l'allocation a marché ou non
    {
        exit(0); // On arrête tout
    }

    while (val > 0)
    {   
        reste = val % 2;
        val = val / 2 ;
        
        resultat[i] = reste + '0' ;
        i++;
    }

    inversionResultat = inverse(resultat);
    
    return inversionResultat;
    free(resultat);
    free(inversionResultat);
}


char *dectobin_v3(unsigned int val)
{
    char *resultat, *inversionResultat = NULL; 
    unsigned bit = 0 ;
	unsigned mask = 1 ;

    resultat =  malloc(sizeof(val)*4 * sizeof(char));
    inversionResultat = malloc(sizeof(val)*4  * sizeof(char));

	for (int i = 0 ; i < (sizeof(val)*4 ); ++i)
	{
		bit = (val&mask) >> i ;
		resultat[i] = bit + '0' ;
		mask <<= 1 ;
	}

    inversionResultat = inverse(resultat);
    
    return inversionResultat;
    free(inversionResultat);
    free(resultat);
}

char *dectohexa_v2(unsigned int val){
    char *resultat, *inversionResultat = NULL; 
    char tempValBit; 
    unsigned bit = 0 ;
	unsigned mask = 15 ; // Nombre de F le max sur un hexa
    int p = 0, taille = 0; 
    unsigned int tempValTaille1 = val; 
    // Nombre de chiffre dans le nombre : 
    if (tempValTaille1 >= 10)
    {
        while(tempValTaille1 > 0){
            taille ++;
            tempValTaille1 = tempValTaille1 / 10 ;
        }
    }
    else {
        taille = 1;
    }

    resultat =  malloc((taille+1) * sizeof(char));
    inversionResultat = malloc((taille+1 )* sizeof(char));
    

	for (int i = 0 ; i < (taille*4+1); i+=4) // decalage de 4bits par 4bits 
	{
		bit = (val & mask) >> i ;
        tempValBit = bit + '0';

        // Passage en lettre : 
        if (tempValBit >= 58 && tempValBit<=63)
        {
            tempValBit+= (65-58); // ascii : 'A' - ':'
        }
		resultat[p] =  tempValBit;
		mask <<= 4 ;
        p ++;
	}

    inversionResultat = inverse(resultat);
    
    return inversionResultat;
    free(inversionResultat);
    free(resultat);
}

